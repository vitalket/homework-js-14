window.addEventListener('DOMContentLoaded', () => {

const btnDark = document.querySelector('.dark-mode');
const body = document.body;

function changeButton(mode) {
    if (mode === 'dark') {
        btnDark.style.color = '#e5e5e5';
        btnDark.style.backgroundColor = '#323232';
        btnDark.textContent = 'dark mode';
    } else {
        btnDark.style.color = '#323232';
        btnDark.style.backgroundColor = '#e5e5e5';
        btnDark.textContent = 'light mode';
    }
}

function changeMode() {
    const mode = body.classList.contains('dark') ? 'light' : 'dark';
    body.classList.toggle('dark');
    localStorage.setItem('theme', mode);
    changeButton(mode);
}

const savedTheme = localStorage.getItem('theme');

if (savedTheme === 'dark') {
    body.classList.add('dark');
    changeButton('dark');
} else {
    changeButton('light');
}

btnDark.addEventListener('click', changeMode);

});
